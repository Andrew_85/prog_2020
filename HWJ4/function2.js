
/*Функция, которая принимает введённое от пользователя значение, 
а затем проверяет его с помощью тернарного оператора на соответсвие 
определённому условию (>18 или нет) и возвращает true или false*/
function checkAge() {
    
    const year = +prompt("Введите свой возвраст");
    const age = (year > 18) ?  true : confirm ("Родители разрешили?")
    
    document.write(age);
    return age;
   
}
//Вызов этой функции
checkAge();

//Та же функция, но проверяет условие с помощью условного оператора "или"
function checkAge() {
    
    const year = +prompt("Введите свой возвраст");
      let age1 = ((year > 18) ||  (confirm("Родители разрешили?")) == true); 
          if (age1) {
    document.write(age1)
          }
          else document.write(false);
    return age1;
   }
//Вызов этой функции
     checkAge()