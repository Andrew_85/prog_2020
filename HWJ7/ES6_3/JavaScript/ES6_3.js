

// Произвольный массив
const a = ["Hello", "world", 23, null, "23", true, 456, NaN, undefined, false]
// Функция, которая принимает массив и тип элементов
filterBy = (arr, type) => arr.filter(item => typeof item !== type)
//Вывожу в консоль результат работы функции(для элементов, кроме чисел)
console.log(filterBy(a, "number"))