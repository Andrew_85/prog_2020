//Создаю класс Human 
class Human{
    //Это свойства экземпляра класса
    constructor (name, lastname, day, sex) {
        this.name = name;
        this.lastname = lastname;
        this.day = day;
        this.sex = sex;
    }
    //А это методы экземпляра класса
        say = function() {
            alert(`Привет, меня зовут ${this.name} и я отработал(а) в 
            этом месяце  ${this.day} дней`)
        }
        getWages = function() {
            
             if (this.sex == "m") {
                return this.day*Human.m
            }
                   else {return this.day*Human.w}
         }
       }

/*Это функция, которая принимает у пользователя количество отработанных дней 
и передаёт это количество аргументом при создании экземпляра класса*/
    function Day() {
        return +prompt("введите количество рабочих дней")
    }
//Это свойства функции конструктора
Human.m = 2000;
Human.w = 1500;

//А это метод функции конструктора
Human.sayHello = () => {
    console.log("Привет, я - метод функции-конструктора Human")
}

const person1 = new Human("Ivan", "Ivanov", Day(), "m");
const person2 = new Human("Ira", "Andreeva", Day(), "w");
//Вызов методов экземпляров
person1.say()
person2.say()
console.log(person1, person1.getWages())
console.log(person2, person2.getWages())

//Вызов метода конструктора
Human.sayHello()


