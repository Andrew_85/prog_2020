//Создаю конструктор объектов
function Human(name, age, sex) {
    this.name = name;
    this.age = age;
    this.sex = sex;
    this.say = function() {
        alert(`Hello, my name is ${this.name}, I am ${age} years old`)
    }
};
//С помощью конструктора создаю 6 объектов
const human1 = new Human("Max", 35, "man");
const human2 = new Human("Alyona", 39, "woman");
const human3 = new Human("Igor", 30, "man");
const human4 = new Human("Masha", 25, "woman");
const human5 = new Human("Milana", 28, "woman");
const human6 = new Human("Valik", 37, "man");
//Заполняю объектами массив
const allHuman = [human1, human2, human3, human4, human5, human6];
//Функция, которая сортирует по возрасту элементы массива по уменьшению
function sortByAge(allHuman) {
    allHuman.sort((a, b) => a.age > b.age ? -1 : 1);
  }
  sortByAge(allHuman);

document.write(allHuman[0].name+"<br>");
document.write(allHuman[1].name+"<br>");
document.write(allHuman[2].name+"<br>");
document.write(allHuman[3].name+"<br>");
document.write(allHuman[4].name+"<br>");
document.write(allHuman[5].name+"<br>");
